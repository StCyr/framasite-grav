---
menu: Clients
---

[g-clients attributes="id:_clients,class:clients module"]

## Clients / Revue de presse / Références
Utilisez le module `clients` pour montrer à vos visiteurs par exemple quelles sont les clients qui vous font confiance ou bien quels sont les médias qui ont parlé de votre projet

___

[g-clients-item image="gnu.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="arch.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="debian.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="fedora.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="linux.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="mint.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="opensuse.png" attributes="class:col-md-3"][/g-clients-item]
[g-clients-item image="ubuntu.png" attributes="class:col-md-3"][/g-clients-item]

[/g-clients]